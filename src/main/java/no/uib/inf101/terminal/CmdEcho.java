package no.uib.inf101.terminal;

public class CmdEcho implements Command {

    @Override
    //run(String[]) returnere en streng hvor hvert av argumentene i String[] arrayen er limt sammen med mellomrom. Gå gjennom args med en foreach-løkke, og legg til et mellomrom etter hver av strengene. 
    public String run(String[] args) {
        String result = "";
        for (String arg : args) {
            result += arg + " ";
         }
            return result; 
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return "echo";
    }
    
}
